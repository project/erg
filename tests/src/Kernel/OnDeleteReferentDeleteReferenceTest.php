<?php

namespace Drupal\Tests\erg\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Tests entity reference deletion.
 *
 * @group ERG
 */
class OnDeleteReferentDeleteReferenceTest extends EntityKernelTestBase {

  /**
   * The modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'user',
    'erg',
    'erg_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('erg_test_odrdreferee');
    $this->installEntitySchema('erg_test_odrdreference');
    $this->installEntitySchema('erg_test_odrpreferent');
    $this->installEntitySchema('erg_test_opvrrac');

    $this->installConfig(['user']);
  }

  /**
   * Tests entity reference deletion.
   */
  public function testMyTest() {
    $storage = \Drupal::entityTypeManager()->getStorage('erg_test_odrdreference');
    $user = $this->createUser();
    /** @var \Drupal\erg_test\Entity\OnDeleteReferentDeleteReference $referee */
    $referee = $storage->create();
    $referee->get('users')->appendItem($user);
    $this->assertEmpty($referee->validate());
    $referee->save();

    // Confirm the reference was saved.
    $referee = $storage->loadUnchanged($referee->id());
    $users = $referee->get('users')->referencedEntities();
    $this->assertSame($user->id(), $users[0]->id());

    $user->delete();

    // Confirm the reference was deleted.
    $referee = $storage->loadUnchanged($referee->id());
    $this->assertTrue($referee->get('users')->isEmpty());
  }

}
